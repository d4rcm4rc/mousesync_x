/*
Copyright (C) 2022-2023 Marc André Wittorf

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "x_input_watcher.h"
#include <stdio.h>
#include <uv.h>

#define PROCESS_STATE_IDLE 0
#define PROCESS_STATE_RUNNING 1
#define PROCESS_STATE_RUNNING_RETRIGGER 2
typedef struct app_data_s {
	x_input_watcher_t input_watcher;
	uv_poll_t input_watcher_poll;
	uv_timer_t input_watcher_timer;
	uv_process_t process;
	int process_state;
	char **program_argv;
} app_data_t;
static void watcher_poll_callback(uv_poll_t *poll, int status, int events);

int main(int argc, char *argv[]) {
	uv_loop_t *loop;
	app_data_t app_data = {0};

	loop = uv_default_loop();

	if (argc < 2) {
		fprintf(stderr, "Usage: %s <command> [args...]\n", argv[0]);

		return 1;
	}

	app_data.program_argv = argv + 1;

	x_input_watcher_init(&app_data.input_watcher);
	x_input_watcher_start(&app_data.input_watcher, NULL);
	uv_poll_init(
		loop,
		&app_data.input_watcher_poll,
		x_input_watcher_get_fd(&app_data.input_watcher)
	);
	uv_handle_set_data(
		(uv_handle_t *)&app_data.input_watcher_poll, &app_data
	);
	uv_poll_start(
		&app_data.input_watcher_poll, UV_READABLE, watcher_poll_callback
	);
	uv_timer_init(loop, &app_data.input_watcher_timer);
	uv_handle_set_data(
		(uv_handle_t *)&app_data.input_watcher_timer, &app_data
	);

	return uv_run(loop, UV_RUN_DEFAULT);
}

static void watcher_timer_callback(uv_timer_t *timer);
static void on_new_pointer(app_data_t *app_data);
static void work_watcher(app_data_t *app_data, int wakeup_type) {
	int result = 0;

	result = x_input_watcher_check_next(
		&app_data->input_watcher, wakeup_type
	);

	if (result & X_INPUT_WATCHER_ACTION_REARM_TIMER) {
		uv_timer_start(
			&app_data->input_watcher_timer,
			watcher_timer_callback,
			200,
			0
		);
	}

	if (result & X_INPUT_WATCHER_ACTION_NEW_POINTER) {
		on_new_pointer(app_data);
	}
}

static void watcher_poll_callback(uv_poll_t *poll, int status, int events) {
	app_data_t *app_data = uv_handle_get_data((uv_handle_t *)poll);
	work_watcher(app_data, X_INPUT_WATCHER_WAKEUP_SOCKET);
}

static void watcher_timer_callback(uv_timer_t *timer) {
	app_data_t *app_data = uv_handle_get_data((uv_handle_t *)timer);
	work_watcher(app_data, X_INPUT_WATCHER_WAKEUP_TIMER);
}

static void on_process_exit(
	uv_process_t *process, int64_t exit_status, int term_signal
);
static void do_run_process(app_data_t *app_data) {
	uv_stdio_container_t child_stdio[3];
	child_stdio[0].flags = UV_INHERIT_FD;
	child_stdio[0].data.fd = 0;
	child_stdio[1].flags = UV_INHERIT_FD;
	child_stdio[1].data.fd = 1;
	child_stdio[2].flags = UV_INHERIT_FD;
	child_stdio[2].data.fd = 2;

	uv_process_options_t options = {0};
	options.args = app_data->program_argv;
	options.file = app_data->program_argv[0];
	options.exit_cb = on_process_exit;
	options.stdio_count = 3;
	options.stdio = child_stdio;

	uv_handle_set_data((uv_handle_t *)&app_data->process, app_data);
	uv_spawn(uv_default_loop(), &app_data->process, &options);
}

static void on_new_pointer(app_data_t *app_data) {
	switch (app_data->process_state) {
	case PROCESS_STATE_IDLE:
		do_run_process(app_data);
		app_data->process_state = PROCESS_STATE_RUNNING;
		return;
	case PROCESS_STATE_RUNNING:
		app_data->process_state = PROCESS_STATE_RUNNING_RETRIGGER;
		return;
	case PROCESS_STATE_RUNNING_RETRIGGER:
		return;
	}
}

static void on_process_exit(
	uv_process_t *process, int64_t exit_status, int term_signal
) {
	app_data_t *app_data = uv_handle_get_data((uv_handle_t *)process);

	switch (app_data->process_state) {
	case PROCESS_STATE_IDLE:
		// Shouldn't happen...
		return;
	case PROCESS_STATE_RUNNING:
		app_data->process_state = PROCESS_STATE_IDLE;
		return;
	case PROCESS_STATE_RUNNING_RETRIGGER:
		do_run_process(app_data);
		app_data->process_state = PROCESS_STATE_RUNNING;
		return;
	}
}
