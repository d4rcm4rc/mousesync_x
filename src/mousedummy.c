/*
Copyright (C) 2022-2023 Marc André Wittorf

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <libevdev/libevdev-uinput.h>
#include <libevdev/libevdev.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	// printf("Hello World! \n");
	int err;
	struct libevdev *dev;
	struct libevdev_uinput *uidev;

	dev = libevdev_new();
	libevdev_set_name(dev, "test mouse");
	libevdev_enable_event_type(dev, EV_REL);
	libevdev_enable_event_code(dev, EV_REL, REL_X, NULL);
	libevdev_enable_event_code(dev, EV_REL, REL_Y, NULL);
	libevdev_enable_event_type(dev, EV_KEY);
	libevdev_enable_event_code(dev, EV_KEY, BTN_LEFT, NULL);
	libevdev_enable_event_code(dev, EV_KEY, BTN_MIDDLE, NULL);
	libevdev_enable_event_code(dev, EV_KEY, BTN_RIGHT, NULL);

	err = libevdev_uinput_create_from_device(
		dev, LIBEVDEV_UINPUT_OPEN_MANAGED, &uidev
	);

	if (err) return err;

	libevdev_free(dev);

	sleep(1);

	libevdev_uinput_destroy(uidev);
	uidev = NULL;
}
