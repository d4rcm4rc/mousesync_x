/*
Copyright (C) 2022-2023 Marc André Wittorf

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "x_input_watcher.h"

#include <X11/Xlib-xcb.h>
#include <X11/Xlib.h>
#include <X11/extensions/XInput.h>
#include <stdlib.h>
#include <string.h>
#include <xcb/xcb.h>
#include <xcb/xcb_aux.h>
#include <xcb/xcbext.h>

/*
The watcher can have one of XXXXX states. They are encoded implcitly. This is
what they mean:

* Idle: nothing of interest is happening
* Debounce-waiting: We just got a device event and wait for possible further
  events before we check them. The application is supposed to call us again
  later.
* Waiting for reply: We collected a few devices and requested a device list
* Waiting for reply (invalidated): We're waiting for a reply but in the mean
  time we got more new devices. As we can't be sure whether these devices will
  be included in our already-requested list, we'll request it again
*/

#define PENDING_STATE_NONE 0
#define PENDING_STATE_PENDING 1
#define PENDING_STATE_INVALID 2

#define DEVICE_ID_TO_INDEX(device_id)                                          \
	(device_id / sizeof(X_INPUT_WATCHER_NEW_DEVICE_MASK_T))
#define DEVICE_ID_TO_BIT(device_id)                                            \
	(1 << (device_id % sizeof(X_INPUT_WATCHER_NEW_DEVICE_MASK_T)))

#define ADD_DEVICE_ID(mask, device_id)                                         \
	((mask)[DEVICE_ID_TO_INDEX(device_id)] |= DEVICE_ID_TO_BIT(device_id))

#define HAS_DEVICE_ID(mask, device_id)                                         \
	(((mask)[DEVICE_ID_TO_INDEX(device_id)] & DEVICE_ID_TO_BIT(device_id)  \
	 ) != 0)

static int handle_event(x_input_watcher_t *watcher, xcb_generic_event_t *evt);
static int handle_reply(
	x_input_watcher_t *watcher, xcb_input_list_input_devices_reply_t *reply
);
static int handle_timer_elapsed(x_input_watcher_t *watcher);

static inline void reset_device_mask(x_input_watcher_t *watcher) {
	memset(watcher->new_device_mask, 0, sizeof(watcher->new_device_mask));
}

void x_input_watcher_init(x_input_watcher_t *watcher) {
	watcher->dpy = NULL;
	watcher->conn = NULL;
	watcher->list_devices_pending = PENDING_STATE_NONE;
	reset_device_mask(watcher);
}

void x_input_watcher_start(
	x_input_watcher_t *watcher, const char *display_name
) {
	xcb_input_event_class_t xiclass;

	watcher->dpy = XOpenDisplay(display_name);
	watcher->conn = XGetXCBConnection(watcher->dpy);
	DevicePresence(watcher->dpy, watcher->input_event_type, xiclass);

	int default_screen_no = DefaultScreen(watcher->dpy);
	xcb_screen_t *screen =
		xcb_aux_get_screen(watcher->conn, default_screen_no);
	xcb_window_t root = screen->root;

	xcb_void_cookie_t ck = xcb_input_select_extension_event_checked(
		watcher->conn, root, 1, &xiclass
	);
	xcb_generic_error_t *err = xcb_request_check(watcher->conn, ck);
	if (err) {
		free(err);
	}
}

int x_input_watcher_get_fd(x_input_watcher_t *watcher) {
	if (watcher->conn == NULL) return -1;

	return xcb_get_file_descriptor(watcher->conn);
}

int x_input_watcher_check_next(x_input_watcher_t *watcher, int wakeup_type) {
	xcb_generic_event_t *evt;
	xcb_input_list_input_devices_reply_t *reply;
	int result = 0;

	if (wakeup_type & X_INPUT_WATCHER_WAKEUP_SOCKET) {
		while (1) {
			reply = NULL;
			evt = NULL;
			if (watcher->list_devices_pending !=
			    PENDING_STATE_NONE) {
				xcb_generic_error_t *err;
				int ret = xcb_poll_for_reply(
					watcher->conn,
					watcher->list_devices_cookie.sequence,
					(void **)&reply,
					&err
				);

				evt = xcb_poll_for_queued_event(watcher->conn);
			} else {
				evt = xcb_poll_for_event(watcher->conn);
			}

			if (evt == NULL && reply == NULL) {
				return result;
			}

			if (reply != NULL) {
				result |= handle_reply(watcher, reply);
				free(reply);
			}

			if (evt != NULL) {
				result |= handle_event(watcher, evt);
				free(evt);
			}
		}
	}

	// It's _probably_ safe to early return here. We don't as it's difficult
	// to reason about and may subtly change

	if (wakeup_type & X_INPUT_WATCHER_WAKEUP_TIMER) {
		result |= handle_timer_elapsed(watcher);
	}

	return result;
}

void x_input_watcher_free(x_input_watcher_t *watcher) {}

static int handle_event(x_input_watcher_t *watcher, xcb_generic_event_t *evt) {
	if (evt->response_type != watcher->input_event_type) {
		return 0;
	}

	xcb_input_device_presence_notify_event_t *dpne =
		(xcb_input_device_presence_notify_event_t *)evt;

	if (HAS_DEVICE_ID(watcher->new_device_mask, dpne->device_id)) {
		return 0;
	}

	ADD_DEVICE_ID(watcher->new_device_mask, dpne->device_id);
	if (watcher->list_devices_pending == PENDING_STATE_PENDING) {
		watcher->list_devices_pending = PENDING_STATE_INVALID;

		// Do not rearm. We'll rearm when we got the reply to avoid
		// a race condition
		return 0;
	}
	return X_INPUT_WATCHER_ACTION_REARM_TIMER;
}

void request_device_list(x_input_watcher_t *watcher) {
	watcher->list_devices_cookie =
		xcb_input_list_input_devices(watcher->conn);
	xcb_flush(watcher->conn);

	watcher->list_devices_pending = PENDING_STATE_PENDING;
}

static int handle_timer_elapsed(x_input_watcher_t *watcher) {
	switch (watcher->list_devices_pending) {
	case PENDING_STATE_NONE:
		// Do a request to list all devices
		break;
	case PENDING_STATE_PENDING:
		// We're already doing a request. No need to do another one
		return 0;
	case PENDING_STATE_INVALID:
		// We did a request but it's outdated already.
		// Don't trigger a new one here but instead wait for
		// the previous one's reply
		return 0;
	default:
		return 0;
	}

	request_device_list(watcher);
	return 0;
}

static inline int is_pointer_device(const xcb_input_device_info_t *info) {
	return info->device_use == IsXPointer ||
	       info->device_use == IsXExtensionPointer;
}

static inline int has_new_pointer_device(
	x_input_watcher_t *watcher, xcb_input_list_input_devices_reply_t *reply
) {
	xcb_input_device_info_iterator_t iterator =
		xcb_input_list_input_devices_devices_iterator(reply);

	while (iterator.rem) {
		if (HAS_DEVICE_ID(
			    watcher->new_device_mask, iterator.data->device_id
		    ) &&
		    is_pointer_device(iterator.data)) {
			return X_INPUT_WATCHER_ACTION_NEW_POINTER;
		}
		xcb_input_device_info_next(&iterator);
	}

	return 0;
}

static int handle_reply(
	x_input_watcher_t *watcher, xcb_input_list_input_devices_reply_t *reply
) {
	switch (watcher->list_devices_pending) {
	case PENDING_STATE_NONE:
		// A reply without a request? shouldn't happen
		return 0;
	case PENDING_STATE_PENDING:
		// This is the expected reply for this request
		break;
	case PENDING_STATE_INVALID:
		request_device_list(watcher);

		return 0;
	default:
		return 0;
	}

	int result = has_new_pointer_device(watcher, reply);

	watcher->list_devices_pending = PENDING_STATE_NONE;
	reset_device_mask(watcher);

	return result;
}
