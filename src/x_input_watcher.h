/*
Copyright (C) 2022-2023 Marc André Wittorf

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef X_INPUT_WATCHER_H
#define X_INPUT_WATCHER_H
#pragma once

#include <X11/Xlib.h>
#include <xcb/xcb.h>
#include <xcb/xinput.h>

#define X_INPUT_WATCHER_NEW_DEVICE_MASK_T uint64_t

typedef struct x_input_watcher_s {
	Display *dpy;
	xcb_connection_t *conn;
	int input_event_type;
	X_INPUT_WATCHER_NEW_DEVICE_MASK_T new_device_mask
		[256 / sizeof(X_INPUT_WATCHER_NEW_DEVICE_MASK_T)];
	xcb_input_list_input_devices_cookie_t list_devices_cookie;
	int list_devices_pending;
} x_input_watcher_t;

void x_input_watcher_init(x_input_watcher_t *watcher);
void x_input_watcher_start(
	x_input_watcher_t *watcher, const char *display_name
);
int x_input_watcher_get_fd(x_input_watcher_t *watcher);

#define X_INPUT_WATCHER_WAKEUP_SOCKET (1 << 0)
#define X_INPUT_WATCHER_WAKEUP_TIMER (1 << 1)
#define X_INPUT_WATCHER_ACTION_NEW_POINTER (1 << 0)
#define X_INPUT_WATCHER_ACTION_REARM_TIMER (1 << 1)
int x_input_watcher_check_next(x_input_watcher_t *watcher, int wakeup_type);
void x_input_watcher_free(x_input_watcher_t *watcher);

#endif // X_INPUT_WATCHER_H
