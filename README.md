Mousesync X
===========

This tool runs a program whenever a new pointer device is added to the X server.


Requirements
------------

* a C compiler supporting C11
* pkgconfig
* cmake 3.6 or later with pkgconfig support
* libuv
* libxcb
* libXi

Optionally, for the `mousedummy` tool to simulate a new mouse being
plugged in, libevdev.


Installing
----------

This is a simple cmake program, just install it as usual. Eg.:

    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release -GNinja ..
    ninja
    ninja install/strip

Usage
-----

Just run `mousesync_x <program> [args...]`.  
The program can be any binary or script. It will execute whenever a new pointer
is available.
Example:

    mousesync_x ~/bin/setMouseSpeeds.sh --new-pointer

In this example, we'd run `~/bin/setMouseSpeeds.sh --new-pointer` whenever
there is a new pointer.

Note that the program _won't_ be called when we start the program.
If this is required, just run the program once manually:

    ~/bin/setMouseSpeeds.sh --initial; mousesync_x ~/bin/setMouseSpeeds.sh --new-pointer

Rationale is that often we want different behaviors initially and when a new
pointer device is available.

Note that mousesync_x is very quick.
If we use this to overwrite settings that our desktop environment applies,
our script is probably done when the desktop environment does its routine.
In this case we should have our script wait or reapply its change a few times.
See below for an example.

Sometimes we can't use multiple commands.
Eg. some DE's startup files are not interpreted by a shell.
In that case we can use something like this:

    sh -c '~/bin/setMouseSpeeds.sh --initial; exec mousesync_x ~/bin/setMouseSpeeds.sh --new-pointer'

Note the `exec`, with it we'll avoid keeping a useless `sh` process open.


Debouncing and Queuing
----------------------

It is generally safe to have the script run a bit longer.  
If multiple pointers appear quickly after each other or when our script is
still running when a new pointer appears, `mousesync_x` will just run the
script again.

Note that this doesn't accumulate.
If three pointers appear in quick succession while the script is running,
it will be queued again only once.


Mousedummy
----------

We include the tool `mousedummy`, to build it, configure with

    -DBUILD_MOUSEDUMMY=ON

Now we can just run `mousedummy` and it will attach a pointer device
called _test mouse_, wait a second and detach it again.
This is enough for `mousesync_x` to trigger our program.

See also _mousedummy.c_.


Example for a "Set Speeds" Script
---------------------------------

```bash
#!/bin/env bash

# when run with the '--sync' argument, apply a few times to
# 'win' over the DE's changes to the mouse speed
if [ x"$1" == x"--sync" ]; then
	"$0"
	sleep 0.1
	"$0"
	sleep 0.2
	"$0"
	sleep 0.4
	"$0"
	sleep 0.8
fi

# Now apply our settings
xinput set-prop 'Gaming Mouse Wireless Rodent 1337' 'libinput Accel Profile Enabled' 0 1 2>/dev/null
```
